//TODO check if need to ignore to GIT
const functions = require('firebase-functions');
const admin = require('firebase-admin');

exports.handler = functions.firestore.document('/Polls/{pollId}/responses/{userId}').onCreate((data, context) => {
                  const answerSelected = data.data().answer;

                  const answerRef = admin.firestore().doc(`Polls/${context.params.pollId}/answers/${answerSelected}`);
                  const voteCountRef = admin.firestore().doc(`Polls/${context.params.pollId}`);

                   return admin.firestore().runTransaction(t => {
                               return t.get(answerRef)
                                   .then(doc => {
                                       if (doc.data()) {
                                           t.update(answerRef, { vote_count: doc.data().vote_count + 1 });
                                       }
                                   })
                           }).then(result => {
                               return admin.firestore().runTransaction(t => {
                                           return t.get(voteCountRef)
                                               .then(doc => {
                                                   if (doc.data()) {
                                                       t.update(voteCountRef, {vote_count:doc.data().vote_count+1});
                                                   }
                                               });
                                        });
                           //starting with this set, I believe this code has caused the issue
                           }).then(result => {
                               return admin.firestore().runTransaction(t => {
                                           return t.get(voteCountRef)
                                                .then(doc => {
                                                  if (doc.data()) {
                                                      t.update(voteCountRef, {trend_score:doc.data().trend_score+1});
                                                  }
                                                });
                                        });
                 });
           });