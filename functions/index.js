const functions = require('firebase-functions');
const trackVote = require('./trackVote')
const admin = require('firebase-admin');
admin.initializeApp();

exports.trackVote = trackVote.handler;