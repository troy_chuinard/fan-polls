package com.troychuinard.fanpolls;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TableLayout;
import android.widget.Toast;
import android.widget.ViewAnimator;

import com.facebook.FacebookSdk;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileActivity extends AppCompatActivity {

    private ViewAnimator mViewAnimator;
    private TableLayout mTabLayout;
    private CircleImageView mProfileImage;

    private FirebaseStorage mStorage;
    private StorageReference mFirebaseStorageRef;
    private StorageReference mFileRef;
    private String mResultImageURL;
    private FirebaseUser mUserID;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_profile);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mProfileImage = (CircleImageView) findViewById(R.id.profile_image);
        mStorage = FirebaseStorage.getInstance();
        mFirebaseStorageRef = mStorage.getReferenceFromUrl("gs://firebase-fan-polls.appspot.com");

//        Uri facebookImageURL = FirebaseAuth.getInstance().getCurrentUser().getPhotoUrl();
//        mUserID = FirebaseAuth.getInstance().getCurrentUser();
//        String convertedImageURL = facebookImageURL.toString();
//        Log.v("IMAGE", facebookImageURL.toString());
//
//
//        String userId = String.valueOf(FirebaseAuth.getInstance().getCurrentUser());
//        new RetrieveFeedTask().execute();
//        Picasso.with(getApplicationContext()).load(mResultImageURL).into(mProfileImage);


    }

//    class RetrieveFeedTask extends AsyncTask<String, String, Bitmap> {
//
//        @Override
//        protected Bitmap doInBackground(String... strings) {
//
//            Bitmap bitmap = null;
//            try {
//                String u = "https://graph.facebook.com/" + mUserID.toString() + "/picture?type=large";
//                URL url = new URL(u);
//                bitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            return bitmap;
//        }
//
//        @Override
//        protected void onPostExecute(Bitmap bitmap) {
//            super.onPostExecute(bitmap);
//            ByteArrayOutputStream baos = new ByteArrayOutputStream();
//            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
//            byte[] data = baos.toByteArray();
//
//            String uniqueID = java.util.UUID.randomUUID().toString();
//            mFileRef = mFirebaseStorageRef.child(uniqueID);
//            UploadTask uploadTask = mFileRef.putBytes(data);
//            uploadTask.addOnFailureListener(new OnFailureListener() {
//                @Override
//                public void onFailure(@NonNull Exception exception) {
//                    Toast.makeText(getApplicationContext(), "Error Loading Photo", Toast.LENGTH_LONG).show();
//                }
//            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
//                @Override
//                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
//                    // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
//
//                    Uri downloadUrl = taskSnapshot.getDownloadUrl();
//                    mResultImageURL = downloadUrl.toString();
//
//                }
//            });
//
//        }
//    }
}