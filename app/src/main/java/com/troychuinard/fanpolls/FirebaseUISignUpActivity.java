package com.troychuinard.fanpolls;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.IdpResponse;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.firestore.FirebaseFirestore;
import com.troychuinard.fanpolls.Fragment.DisplayNameDialogFragment;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FirebaseUISignUpActivity extends AppCompatActivity implements DisplayNameDialogFragment.EditNameDialogListener {

    private static final int RC_SIGN_IN = 123;
    private static final String USERS = "Users";

    private FirebaseFirestore mStoreBaseRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mStoreBaseRef = FirebaseFirestore.getInstance();

        // Choose authentication providers
        List<AuthUI.IdpConfig> providers = Arrays.asList(
                new AuthUI.IdpConfig.FacebookBuilder().build(),
                new AuthUI.IdpConfig.EmailBuilder().build());


        startActivityForResult(
                AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setLogo(R.drawable.fan_polls_logo)
                        .setTheme(R.style.UITheme)
                        .setAvailableProviders(providers)
                        .build(),
                RC_SIGN_IN);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            IdpResponse response = IdpResponse.fromResultIntent(data);
            if (resultCode == RESULT_OK) {
                // Successfully signed in
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                for (UserInfo userInfo : FirebaseAuth.getInstance().getCurrentUser().getProviderData()) {
                    if (user.getProviderId().equals("facebook.com")) {
                        System.out.println("User is signed in with Facebook");
                        Map<String, Object> userMap = new HashMap<>();
                        userMap.put("Display_Name", user.getDisplayName());
                        userMap.put("ID", user.getUid());
                        userMap.put("Provider", user.getProviders());
                        mStoreBaseRef.collection(USERS).document(user.getUid()).set(userMap);
                    } else if ("password".equals(user.getProviderData().get(0).getProviderId())) {
                        DialogFragment f = new DisplayNameDialogFragment();
                        f.show(getSupportFragmentManager(), "DisplayNameDialogFragment");
                    }
                }
                Map<String, Object> userMap = new HashMap<>();
                userMap.put("email", user.getEmail());
                userMap.put("display_name", user.getDisplayName());
                userMap.put("user_id", user.getUid());
                userMap.put("provider", user.getProviders());
                mStoreBaseRef.collection(USERS).add(userMap);
                Intent toHome = new Intent(getApplicationContext(), HomeActivity.class);
                toHome.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(toHome);
                finish();
            } else {
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.sign_in_failed),Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onFinishEditDialog(String inputText) {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        final FirebaseUser user = auth.getCurrentUser();
        UserProfileChangeRequest m = new UserProfileChangeRequest.Builder()
                .setDisplayName(inputText).build();
        user.updateProfile(m).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Log.v("INPUT_TEXT", user.getDisplayName());
                Toast.makeText(getApplicationContext(), user.getDisplayName().toString(), Toast.LENGTH_LONG).show();
                Map<String, Object> userMap = new HashMap<>();
                userMap.put("email", user.getEmail());
                userMap.put("display_name", user.getDisplayName());
                userMap.put("user_id", user.getUid());
                userMap.put("provider", user.getProviders());
                mStoreBaseRef.collection(USERS).add(userMap);
            }
        });

        Intent toHome = new Intent(FirebaseUISignUpActivity.this, HomeActivity.class);
        startActivity(toHome);
    }
}
