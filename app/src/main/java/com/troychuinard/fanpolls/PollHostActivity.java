package com.troychuinard.fanpolls;

import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.ListView;



import com.troychuinard.fanpolls.Fragment.DiscussionFragment;
import com.troychuinard.fanpolls.Fragment.PollFragment;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;

public class PollHostActivity extends AppCompatActivity implements PollFragment.OnFragmentInteractionListener, DiscussionFragment.OnFragmentInteractionListener {

    private Toolbar toolbar;

    private ViewPager mPager;
    private SectionPagerAdapter mPagerAdapter;
    private static final int NUMVIEWS = 2;

//    private ScreenSlidePagerAdapter mPagerAdapter;
    private DateFormat mDateFormat;
    private Date mDate;
    private String mCurrentDateString;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    private boolean isOpen;
    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    private Intent mIntentFromTouch;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_poll);
        toolbar = (Toolbar) findViewById(R.id.action_tool_bar);
        toolbar.setTitleTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
        setSupportActionBar(toolbar);
        setTitle(R.string.app_name);
        Window window = getWindow();
        //TODO: Bugging out on API < 21, figure out how to set status bar color in previous Android versions
//        window.setStatusBarColor(getResources().getColor(R.color.actionRed));



        mIntentFromTouch = getIntent();

        final android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
        }

        mTabLayout = (TabLayout) findViewById(R.id.tab_layout);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                toolbar,  /* nav drawer icon to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description */
                R.string.drawer_close  /* "close drawer" description */
        ) {

            /**
             * Called when a drawer has settled in a completely closed state.
             */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                isOpen = false;
            }

            /**
             * Called when a drawer has settled in a completely open state.
             */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                isOpen = true;
            }
        };
        mDrawerToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        mDrawerToggle.setDrawerIndicatorEnabled(false);
        mDrawerLayout.addDrawerListener(mDrawerToggle);

        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        ArrayList<String> drawerTitleArray = new ArrayList<>();
        drawerTitleArray.add(0, "TEST");
        drawerTitleArray.add(1, "TEST 1");
        // Set the adapter for the list view
        mDrawerList.setAdapter(new ArrayAdapter<String>(this,
                R.layout.drawer_list_item, drawerTitleArray));


        // TODO: Add Fragment Code to check if savedInstanceState == null; add at Activity Level?
        // Check that the activity is using the layout version with
        // the fragment_container FrameLayout


        //set up viewpager for current day
        mPager = (ViewPager) findViewById(R.id.poll_fragment_container);
        mPagerAdapter = new SectionPagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);


        //get current date to apply to Viewpager
        mViewPager = (ViewPager) findViewById(R.id.poll_fragment_container);
        mViewPager.setAdapter(new SectionPagerAdapter(getSupportFragmentManager()));

        // TODO: Checkn if AuthStateListenerNecessary
        //Determine whether necessary to use an AuthStateListener here
//        mUserRef.addAuthStateListener(new Firebase.AuthStateListener() {
//            @Override
//            public void onAuthStateChanged(AuthData authData) {
//                if (authData == null) {
//                    Intent backToSignIn = new Intent(getApplication(), SignupActivity.class);
//                    startActivity(backToSignIn);
//                    finish();
//                }
//            }
//        })




    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        if (item.getItemId() == android.R.id.home){
            super.onBackPressed();
            return true;}
        else
            return mDrawerToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }



    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        //TODO: Be sure to only allow one instance of each activity
        //TODO:Address mDrawerToggle code from StackOverflow to make sure I am correctly implementing the return to previous activity
        // Sync the toggle state after onRestoreInstanceState has occurred.
//        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    public class SectionPagerAdapter extends FragmentPagerAdapter {

        public SectionPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            final String pollIndex = mIntentFromTouch.getStringExtra("POLL_ID");
            Log.v("PASSED_ID", "The passed ID is " + pollIndex);
            switch (position) {
                case 0:
                    PollFragment pollFragment = PollFragment.newInstance(getApplicationContext(), pollIndex);
                    return pollFragment;
                case 1:
                    DiscussionFragment discussionFragment = DiscussionFragment.newInstance(pollIndex);
                    return discussionFragment;
                default:
                    return new PollFragment();
            }
        }

        @Override
        public int getCount() {
            return NUMVIEWS;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getResources().getString(R.string.poll_text);
                case 1:
                    return getResources().getString(R.string.discussion_text);
                default:
                    return getResources().getString(R.string.poll_text);
            }
        }
    }


}



