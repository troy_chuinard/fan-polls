package com.troychuinard.fanpolls;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.troychuinard.fanpolls.Fragment.DisplayNameDialogFragment;


//prior to migrating Firebase database
public class LoadingActivity extends AppCompatActivity implements DisplayNameDialogFragment.EditNameDialogListener{

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.v("OnCreate_TRIGGERED", "OnCreate HAS BEEN TRIGGERED");
        setContentView(R.layout.activity_loading);


        mAuth = FirebaseAuth.getInstance();

        final Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    synchronized (this) {
                        wait(3000);
                    }
                } catch (InterruptedException ex) {
                }

                FirebaseUser mUser = mAuth.getCurrentUser();
                if (mUser != null) {
                    // User is signed in
                    if (mUser.getDisplayName() == null || mUser.getDisplayName().isEmpty()) {
                        
                        
                    } else {
                        Log.v("TAG", "onAuthStateChanged:signed_in:" + mUser.getUid());
                        Intent toHomeActivity = new Intent(LoadingActivity.this, HomeActivity.class);
                        startActivity(toHomeActivity);
                        finish();
                    }

                } else {
                    // User is signed out
                    Log.v("TAG", "onAuthStateChanged:signed_out");
                    Intent toFirebaseSignUpActivity = new Intent(LoadingActivity.this, FirebaseUISignUpActivity.class);
                    startActivity(toFirebaseSignUpActivity);
                    finish();
                }


                FirebaseAuth.getInstance().removeAuthStateListener(mAuthListener);
            }
        };

        thread.start();
        }

    //TODO: Investigate why this must NOT call super in order to prevent bug on API 24
    //TODO: Investigate why different radio buttons on API 24

    @Override
    public void onFinishEditDialog(String inputText) {

        FirebaseAuth auth = FirebaseAuth.getInstance();
        UserProfileChangeRequest m = new UserProfileChangeRequest.Builder()
                .setDisplayName(inputText).build();
            FirebaseUser user = auth.getCurrentUser();
            user.updateProfile(m);
        Log.v("INPUT_TEXT", user.getDisplayName());
        Toast.makeText(getApplicationContext(), inputText, Toast.LENGTH_LONG).show();
        Intent toHome = new Intent(LoadingActivity.this, HomeActivity.class);
        startActivity(toHome);
    }
}



