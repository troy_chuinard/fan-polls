package com.troychuinard.fanpolls.Fragment;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatRadioButton;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.HorizontalBarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;


import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.SetOptions;
import com.squareup.picasso.Picasso;
import com.troychuinard.fanpolls.Model.Answer;
import com.troychuinard.fanpolls.Model.MyDataValueFormatter;
import com.troychuinard.fanpolls.Model.Poll;
import com.troychuinard.fanpolls.Model.User;
import com.troychuinard.fanpolls.R;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PollFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PollFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PollFragment extends Fragment {


    @BindView(R.id.comment_label_counter)
    TextView mCommentCounter;
    @BindView(R.id.comments_label_icon)
    ImageView mCommentsLabelIcon;
    private FirebaseAuth mAuth;
    @BindView(R.id.arrow_right)
    ImageView mRightArrow;



    private FirebaseFirestore mStoreBaseRef;
    private DocumentReference mStoreSelectedPollRef;

    private RadioGroup mPollQuestionRadioGroup;
    private RadioGroup.LayoutParams mParams;
    //static
    private TextView mCommentsLabel;

    private TextView mTotalVoteCounter;
    private TextView mSelectedVote;

    private TextView mYourVotelabel;
    private TextView mDisplayName;
    private ViewPager mViewPager;
    private int mPagerCurrentPosition;
    private String mUserID;
    private String mUserDisplayName;
    private String mPollCreatorID;
    private String mPollCreatorDisplayName;
    private List<String> mFollowers;

    private static final String USERS_LABEL = "Users";
    private static final String USER_ID_LABEL = "user_id";
    private static final String FOLLOWING_LABEL = "following";
    private static final String FOLLOWERS_LABEL = "followers";
    private static final String VOTE_COUNT_LABEL = "vote_count";
    private static final String QUESTION_LABEL = "question";
    private static final String DISPLAY_NAME_LABEL = "display_name";
    private static final String ANSWERS_LABEL = "answers";
    private static final String POLL_LABEL = "Polls";
    private static final String IMAGE_URL = "image_URL";
    private static final String RESPONSES_LABEL = "responses";

    //all date items; dynamic
    private DateFormat mDateFormat;
    private Date mDate;
    private String mCurrentDateString;
    private TextView mPollQuestion;
    private ArrayList<RadioButton> mPollAnswerArrayList;
    private HorizontalBarChart mPollResults;
    ArrayList<BarEntry> pollResultChartValues;
    private BarDataSet data;
    private ArrayList<IBarDataSet> dataSets;
    private String pollID;
    private int mPollIndex;
    private ProgressBar mProgressBar;
    private CheckBox mFollowingCheck;
    private static Context mContext;

    private OnFragmentInteractionListener mListener;

    private ListenerRegistration x;

    public PollFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment PollFragment.
     */
    // TODO: Rename and change types and number of parameters
    // TODO: Decide where to add comments button;
    public static PollFragment newInstance(Context context, String pollIndex) {
        PollFragment fragment = new PollFragment();
        Bundle args = new Bundle();
        args.putString("POLL_ID", pollIndex);
        fragment.setArguments(args);
        mContext = context;
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //TODO: check navigation to see if there are different ID's being generated from trending, following, and new fragments
        Bundle args = getArguments();
        pollID = args.getString("POLL_ID");
        Log.v("TAG", "THE PASSED ID Is " + pollID);
        mAuth = FirebaseAuth.getInstance();
        mUserID = mAuth.getCurrentUser().getUid();


        mStoreBaseRef = FirebaseFirestore.getInstance();
        mStoreSelectedPollRef = mStoreBaseRef.collection(POLL_LABEL).document(pollID);
//        mStoreSelectedPollRef = mStoreBaseRef.document(POLL_LABEL);



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO: Add Fragment Code to check if savedInstanceState == null; add at Activity Level?


        // Inflate the layout for this fragment
        final ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_poll, container, false);
        ButterKnife.bind(this, rootView);
        getActivity().setTitle(R.string.todays_polls_title);

        //Initialize Poll Results Bar Chart and set to Invisible
        mPollResults = (HorizontalBarChart) rootView.findViewById(R.id.poll_results_chart);
        mPollResults.setBackgroundColor(getResources().getColor(R.color.white));
        mPollResults.setNoDataTextDescription(getResources().getString(R.string.no_results_description));
        mPollResults.setVisibility(View.INVISIBLE);
        mViewPager = getActivity().findViewById(R.id.poll_fragment_container);

        RecyclerView m = new RecyclerView(getContext());


//        ArrayList<BarEntry> pollResultChartValues = new ArrayList<BarEntry>();
        mTotalVoteCounter = (TextView) rootView.findViewById(R.id.total_vote_counter);
        mCommentCounter = (TextView) rootView.findViewById(R.id.comment_label_counter);
        mDisplayName = (TextView) rootView.findViewById(R.id.creator_id);

        mProgressBar = (ProgressBar) rootView.findViewById(R.id.progress_bar_white);

        mPollQuestion = (TextView) rootView.findViewById(R.id.poll_question);
        mPollQuestion.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.poll_question_text_size));
        mPollQuestionRadioGroup = (RadioGroup) rootView.findViewById(R.id.poll_question_group);
        mFollowingCheck = (CheckBox) rootView.findViewById(R.id.following_check);


        // TODO: Add security measures to only allow users to write to Vote Counts;
        // TODO: Check on FragmentStatePagerAdapter and FragmentPagerAdapter; check use of setOffScreenPageLimit() method;
        //     TODO:  the issue lies in the fact that views are continuously being created and destroyed in the viewpager
        //TODO: Prevent same activity from opening twice as it allows for multiple user votes
        //TODO: Add spinner after vote selection
        //TODO: Add spinner after vote selection



        mStoreSelectedPollRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(final DocumentSnapshot documentSnapshot) {
                Poll selectedPoll = documentSnapshot.toObject(Poll.class);
                String pollQuestion = selectedPoll.getQuestion();
                mPollQuestion.setText(pollQuestion);
                mPollQuestion.setTypeface(null, Typeface.BOLD);
                mPollCreatorDisplayName = selectedPoll.getDisplay_name();
                mPollCreatorID = selectedPoll.getCreator_ID();
                mFollowers = selectedPoll.getFollowers();


                if (mFollowers != null) {
                    for (String follower : mFollowers) {
                        if (mUserID.equals(follower)) {
                            mFollowingCheck.setChecked(true);
                        }
                    }
                }


                if (mUserID.equals(mPollCreatorID)){
                    mFollowingCheck.setVisibility(View.INVISIBLE);
                    mDisplayName.setVisibility(View.INVISIBLE);
                }
                mDisplayName.setText(mPollCreatorDisplayName);
                mDisplayName.setTypeface(null, Typeface.BOLD);

                String pollImageURL = selectedPoll.getImage_URL();
                Picasso.get()
                        .load(pollImageURL)
                        .fit()
                        .placeholder(R.drawable.loading_spinner_white)
                        .into((ImageView) rootView.findViewById(R.id.poll_image));
                //where I am trying to access the answer count



            }
        });

        //Queries are shallow, must make a second request
        mStoreSelectedPollRef.collection(ANSWERS_LABEL).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                List<Answer> answers = task.getResult().toObjects(Answer.class);
                Collections.reverse(answers);
                addRadioButtonsWithFirebaseAnswers(answers);
            }
        });

        //Queries are shallow, must make a third request
        mStoreSelectedPollRef.collection(RESPONSES_LABEL).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                for ( DocumentSnapshot d : task.getResult()){
                    if (d.getId().equals(mUserID)){
                        mPollQuestionRadioGroup.setVisibility(View.GONE);
                        mPollResults.setVisibility(View.VISIBLE);

                    }
                }
            }
        });


        //set checked to true if already following
        //TODO: Investigate why the checkbox is not changing. See if it has to do with Firebase asynchronous listeners



//TODO: Create user nodes related to total votes and polls created as part of a leaderboard

        //TODO: Ensure you cannot follow yourself
        //TODO: Should this be in onStart() so that poll counts update?
        //TODO: Should this all be a mutable transaction?
        mFollowingCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (mUserID.equals(mPollCreatorID)){
                    return;
                }
                if (b) {
                    //add to followers node for user who created poll
                    final HashMap<String, Object> followersMap = new HashMap<>();
                    followersMap.put("followers", FieldValue.arrayUnion(mUserID));
                    Query x = mStoreBaseRef.collection(USERS_LABEL).whereEqualTo("user_id", mPollCreatorID);
                    x.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            for (DocumentSnapshot d : task.getResult()){
                                mStoreBaseRef.collection(USERS_LABEL).document(d.getId()).update(followersMap);
                            }
                        }

                    });
                    //add to following node for user who checked following box
                    final HashMap<String, Object> followedUser = new HashMap<>();
                    followedUser.put("following", FieldValue.arrayUnion(mPollCreatorID));
                    Query y = mStoreBaseRef.collection(USERS_LABEL).whereEqualTo(USER_ID_LABEL, mUserID);
                    y.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            for (DocumentSnapshot d : task.getResult()){
                                User user = d.toObject(User.class);
                                HashMap<String, Object> followingMap = new HashMap<>();
                                followingMap.put(FOLLOWING_LABEL, FieldValue.arrayUnion(followingMap));
                                mStoreBaseRef.collection(USERS_LABEL).document(d.getId()).update(followedUser);
                            }
                        }
                    });


                } else  {
                    final HashMap<String, Object> followersMap = new HashMap<>();
                    followersMap.put("followers", FieldValue.arrayRemove(mUserID));
                    mStoreBaseRef.collection("Users").document(mPollCreatorID).update(followersMap);
                    Query x = mStoreBaseRef.collection(USERS_LABEL).whereEqualTo("user_id", mPollCreatorID);
                    x.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            for (DocumentSnapshot d : task.getResult()){
                                mStoreBaseRef.collection(USERS_LABEL).document(d.getId()).update(followersMap);
                            }
                        }

                    });

                    final HashMap<String, Object> followedUser = new HashMap<>();
                    followedUser.put("following", FieldValue.arrayRemove(mPollCreatorID));
                    Query y = mStoreBaseRef.collection(USERS_LABEL).whereEqualTo(USER_ID_LABEL, mUserID);
                    y.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            for (DocumentSnapshot d : task.getResult()){
                                User user = d.toObject(User.class);
                                HashMap<String, Object> followingMap = new HashMap<>();
                                followingMap.put(FOLLOWING_LABEL, FieldValue.arrayUnion(followingMap));
                                mStoreBaseRef.collection(USERS_LABEL).document(d.getId()).update(followedUser);
                            }
                        }
                    });

                }
            }
        });

        //TODO: Create FOLLOWERS IN Addition to following

        mPollQuestionRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                disableVoteButtons(group);
                Toast.makeText(getActivity().getApplicationContext(), R.string.vote_submitted_label, Toast.LENGTH_LONG).show();

                voteOnPoll((Integer) group.findViewById(checkedId).getTag());

                //TODO: add animation between between vote click and chart display
                mPollQuestionRadioGroup.setVisibility(View.INVISIBLE);
                mPollResults.setVisibility(View.VISIBLE);
            }

        });


        mParams = new RadioGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        mParams.setMargins(0,0,16,28);
        mCommentsLabelIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mViewPager.setCurrentItem(1);
            }
        });

        mRightArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mViewPager.setCurrentItem(1);
            }
        });
        return rootView;
    }

    private void addToFollower() {
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //Check if this line is even necessary; it is at the ActivityCreated level therefore it may not
        //be worthwhile
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onStart() {
        super.onStart();

        Query pollQuery = mStoreSelectedPollRef.collection(ANSWERS_LABEL);
        x = pollQuery.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(QuerySnapshot querySnapshot, FirebaseFirestoreException e) {
                int totalVoteCount = 0;
                int numberOfAnswers = querySnapshot.size();
                Log.v("number of answers", String.valueOf(numberOfAnswers));
                for (DocumentSnapshot z : querySnapshot) {
                    Log.v("query_Snap", z.getData().toString());
                    Answer answer = z.toObject(Answer.class);
                    int answerAmount = answer.getVote_count();
                    totalVoteCount = totalVoteCount + answerAmount;
                }
                mTotalVoteCounter.setText(String.valueOf(totalVoteCount));
                Map<String, Object> totalVotes = new HashMap<>();
                totalVotes.put(VOTE_COUNT_LABEL, totalVoteCount);
                //TODO: Should be managed through Cloud Firestore
                mStoreSelectedPollRef.set(totalVotes, SetOptions.merge());
                updatePollResultAnswersDynamically(numberOfAnswers, querySnapshot);
            }
        });
    }


    //close event listener onStop
    @Override
    public void onStop() {
        super.onStop();
        x.remove();
        Log.e("ONSTOP", "ONSTOPCALLED");
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

//    public void changeText(String questionText){
//        mNewText = questionText;
//    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void addRadioButtonsWithFirebaseAnswers(List<Answer> answers) {
        mPollAnswerArrayList = new ArrayList<RadioButton>();
        int indexCreated = 0;
        for (Answer singleAnswer : answers) {
            mPollAnswerArrayList.add((indexCreated), new RadioButton(mContext));
            RadioButton radioButton = mPollAnswerArrayList.get(indexCreated);
            radioButton.setTag(indexCreated);
            radioButton.setText(" " + singleAnswer.getAnswer().toString());
            radioButton.setTextColor(getResources().getColor(R.color.black));
            radioButton.setButtonDrawable(R.drawable.custom_btn_radio);
            //TODO: Investigate if this line is necessary
            radioButton.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.radio_button_answer_text_size));

            mPollQuestionRadioGroup.addView(radioButton, mParams);
            indexCreated++;
        }
    }

    private void updatePollResultAnswersDynamically(int numberOfAnswers, QuerySnapshot querySnapshot) {

        mPollResults.setDescription("");
        //TODO: Check if this check is necesary; working fine without it
//        if (mPollResults != null) {
//            mPollResults.clearValues();
//        }

        int indexForAnswer = numberOfAnswers;
        pollResultChartValues = new ArrayList<BarEntry>();
        int barEntryCounter = 0;
        ArrayList<Float> pollValues = new ArrayList<>();
        for (DocumentSnapshot x : querySnapshot) {
            //TODO: Reverse answer order since it is appearing in the chart bottom up
            Answer answer = x.toObject(Answer.class);
            int barEntryValue = answer.getVote_count();
            Float barEntryFloatValue = Float.valueOf(barEntryValue);
            pollValues.add(barEntryFloatValue);
//            BarEntry z = new BarEntry(barEntryFloatValue, barEntryCounter);
//            pollResultChartValues.add(z);
//            barEntryCounter++;
        }
        Collections.reverse(pollValues);
        int counter = 0;
        for (int i = 0; i < numberOfAnswers; i++){
            BarEntry x = new BarEntry(pollValues.get(i), counter);
            pollResultChartValues.add(x);
            counter++;
        }


//        Collections.reverse(pollResultChartValues);
        data = new BarDataSet(pollResultChartValues, "Poll Results");

//        data.setColors(new int[]{getResources().getColor(R.color.black)});
        //TODO: Check attachment to Activity; when adding a color, the getResources.getColor states
        //TODO: that the fragment was detached from the activity; potentially add this method to onCreateView() to avoid;
        ArrayList<Integer> barColors = new ArrayList<>();
        barColors.add(ContextCompat.getColor(getContext(), R.color.bar_one));
        barColors.add(ContextCompat.getColor(getContext(), R.color.bar_two));
        barColors.add(ContextCompat.getColor(getContext(), R.color.bar_three));
        barColors.add(ContextCompat.getColor(getContext(), R.color.bar_four));
        barColors.add(ContextCompat.getColor(getContext(), R.color.bar_five));
        data.setColors(barColors);

        data.setAxisDependency(YAxis.AxisDependency.LEFT);
        MyDataValueFormatter f = new MyDataValueFormatter();
        data.setValueFormatter(f);


        //xAxis is a inverted yAxis since the graph is horizontal
        XAxis xAxis = mPollResults.getXAxis();
        xAxis.setDrawGridLines(false);
        xAxis.setDrawAxisLine(false);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_XLARGE) {
            xAxis.setTextSize(20);
        } else if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE) {
            xAxis.setTextSize(16);
        }

        //yAxis is an inverted xAxis since the graph is horizontal
        YAxis yAxisLeft = mPollResults.getAxisLeft();
        yAxisLeft.setDrawGridLines(false);
        yAxisLeft.setEnabled(false);
        YAxis yAxisRight = mPollResults.getAxisRight();
        yAxisRight.setDrawGridLines(false);
        yAxisRight.setEnabled(false);


        Legend legend = mPollResults.getLegend();
        legend.setEnabled(false);


        //TODO: This figure needs to be dynamic and needs to adjust based on the number of users in the application
        //TODO: Or does it? Right now, it scales without it

        dataSets = new ArrayList<IBarDataSet>();

        dataSets.add(data);


        //Poll Answer Options get added here

        ArrayList<String> yVals = new ArrayList<String>();
        int yValsCounter = 0;
        for (DocumentSnapshot x : querySnapshot) {
            Answer answer = x.toObject(Answer.class);
            yVals.add(yValsCounter, answer.getAnswer());
            yValsCounter++;
        }

//        Collections.reverse(yVals);

        BarData testBarData = new BarData(yVals, dataSets);

        //TODO: Fix all text sizes using this method; only way to fix bar text sizes after extensive research
        if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_XLARGE) {
            testBarData.setValueTextSize(22);
        } else if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE) {
            testBarData.setValueTextSize(14);
        } else if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_NORMAL) {
            testBarData.setValueTextSize(12);
        } else if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_SMALL) {
            testBarData.setValueTextSize(12);
        }

        mPollResults.getXAxis().setLabelRotationAngle(-15);
        mPollResults.getXAxis().setSpaceBetweenLabels(5);
        mPollResults.setTouchEnabled(false);
        mPollResults.setPinchZoom(false);
        mPollResults.setData(testBarData);
        data.notifyDataSetChanged();
        mPollResults.notifyDataSetChanged();
        mPollResults.invalidate();
    }

    private void voteOnPoll(int checkedRadioButtonIndex) {
        int selectedAnswerIndex = checkedRadioButtonIndex + 1;
        HashMap<String, Object> firebaseAnswer = new HashMap<>();
        firebaseAnswer.put("answer", selectedAnswerIndex);
        mStoreBaseRef.collection("Polls").document(pollID).collection("responses").document(mUserID).set(firebaseAnswer);
    }

    private int getItem(int i) {
        return mViewPager.getCurrentItem() + i;
    }

    private void disableVoteButtons(RadioGroup group) {
        //disable voting
        for (int i = 0; i < group.getChildCount(); i++) {
            group.getChildAt(i).setEnabled(false);
        }
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


}


