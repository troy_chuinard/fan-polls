package com.troychuinard.fanpolls.Fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
//import com.google.firebase.database.Query;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.squareup.picasso.Picasso;
import com.troychuinard.fanpolls.CreatePollActivity;
import com.troychuinard.fanpolls.Model.Poll;
import com.troychuinard.fanpolls.PollHostActivity;
import com.troychuinard.fanpolls.R;

import java.util.ArrayList;

/**zs
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {zlink NewFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link NewFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TrendingFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private RecyclerView mRecyclerview;

    private LinearLayoutManager mLayoutManager;
    private FloatingActionButton mFloatingActionAdd;

    private FirebaseFirestore mStoreBaseRef;

    private FirestoreRecyclerAdapter<Poll, PollHolder> mFirestoreAdaper;


    private RecyclerView.ItemAnimator mItemAnimator;

    private ArrayList<PollHolder> mPollHolderArray;

    private Long filterEpochValue;


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public TrendingFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment NewFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TrendingFragment newInstance(String param1, String param2) {
        TrendingFragment fragment = new TrendingFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mStoreBaseRef = FirebaseFirestore.getInstance();

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View v = inflater.inflate(R.layout.fragment_poll_feed, container, false);



        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mRecyclerview = (RecyclerView) view.findViewById(R.id.list);
        mPollHolderArray = new ArrayList<>();
        mRecyclerview.getItemAnimator().setChangeDuration(0);



        mLayoutManager = new LinearLayoutManager(getContext());
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        //TODO: Detertmine if necessary since ordering is now different in Firebase
        mLayoutManager.setReverseLayout(true);
        mLayoutManager.setStackFromEnd(true);

        mFloatingActionAdd = (FloatingActionButton) getActivity().findViewById(R.id.myFAB);
        mFloatingActionAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent I = new Intent(getActivity().getApplicationContext(), CreatePollActivity.class);
                startActivity(I);
            }
        });


        mRecyclerview.setLayoutManager(mLayoutManager);

        Query queryStore = FirebaseFirestore.getInstance()
                .collection("Polls")
                .orderBy("trend_score");

        FirestoreRecyclerOptions<Poll> storeOptions = new FirestoreRecyclerOptions.Builder<Poll>()
                .setQuery(queryStore, Poll.class)
                .build();

        mFirestoreAdaper = new FirestoreRecyclerAdapter<Poll, PollHolder>(storeOptions) {
            @Override
            protected void onBindViewHolder(@NonNull final PollHolder holder, final int position, @NonNull Poll model) {
                holder.mPollQuestion.setText(model.getQuestion());
                String voteCount = String.valueOf(model.getVote_count());
                //TODO: Investigate formatting of vote count for thousands

                holder.mVoteCount.setText(voteCount);
                Picasso.get()
                        .load(model.getImage_URL())
                        .fit()
                        .into(holder.mPollImage);

                holder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent toClickedPoll = new Intent(getActivity(), PollHostActivity.class);
                        String recyclerPosition = getSnapshots().getSnapshot(position).getId();
                        Log.v("Firestore ID", recyclerPosition);
                        toClickedPoll.putExtra("POLL_ID", recyclerPosition);
                        startActivity(toClickedPoll);
                    }
                });
            }

            @Override
            public PollHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.latest_item,parent,false);
                return new PollHolder(v);
            }
        };

        mRecyclerview.setAdapter(mFirestoreAdaper);
        mFirestoreAdaper.startListening();
   }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mFirestoreAdaper.stopListening();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public static class PollHolder extends RecyclerView.ViewHolder {

        TextView mPollQuestion;
        TextView mVoteCount;
        ImageView mPollImage;
        View mView;
        String mTag;


        public PollHolder(View itemView) {
            super(itemView);

            mPollQuestion = (TextView) itemView.findViewById(R.id.latest_item_question);
            mPollImage = (ImageView) itemView.findViewById(R.id.pollThumbNailImage);
            mVoteCount = (TextView) itemView.findViewById(R.id.latest_item_poll_count);
            this.mView = itemView;
        }

        public void setTag(String tag){
            this.mTag = tag;
        }

        public View getViewByTag(String tag){
            return mView;
        }

    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}