package com.troychuinard.fanpolls.Model;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.troychuinard.fanpolls.R;

public class PollHolder extends RecyclerView.ViewHolder {
    private TextView mPollQuestion;
    private ImageView mPollImage;

    public PollHolder(View itemView) {
        super(itemView);
        mPollQuestion = (TextView) itemView.findViewById(R.id.latest_item_question);
        mPollImage = (ImageView) itemView.findViewById(R.id.pollThumbNailImage);
    }

    public void bind(Context context, Poll poll) {
        mPollQuestion.setText(poll.getQuestion());
        Picasso.get()
                .load(poll.getImage_URL())
                .fit()
                .into(mPollImage);
        Log.v("QUESTION", poll.getQuestion());
        Log.v("IMAGE", poll.getImage_URL());
    }
}
