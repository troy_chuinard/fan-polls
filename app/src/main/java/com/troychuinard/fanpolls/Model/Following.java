package com.troychuinard.fanpolls.Model;

import com.google.firebase.firestore.IgnoreExtraProperties;

@IgnoreExtraProperties
public class Following {

    private String user_ID;

    public Following(){

    }

    public Following(String user_id) {
        this.user_ID = user_id;
    }
    public String getUser_id() {
        return user_ID;
    }

    public void setUser_id(String user_id) {
        this.user_ID = user_id;
    }
}
