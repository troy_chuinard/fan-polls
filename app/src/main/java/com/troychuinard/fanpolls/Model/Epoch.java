package com.troychuinard.fanpolls.Model;

/**
 * Created by troychuinard on 2/25/18.
 */

public class Epoch {

    private int filter_value;

    public Epoch(){

    }

    public Epoch (int filter_value){
        this.filter_value = filter_value;
    }


    public int getFilter_value() {
        return filter_value;
    }

    public void setFilter_value(int filter_value) {
        this.filter_value = filter_value;
    }
}
