package com.troychuinard.fanpolls.Model;


import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.IgnoreExtraProperties;
import com.google.firebase.firestore.ServerTimestamp;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@IgnoreExtraProperties
public class Poll {

    private String question;
    private String image_URL;
    private String creator_ID;
    private String display_name;
    private int vote_count;
    private Long epoch;
    private long trend_score;
    private List<String> followers;

    @ServerTimestamp
    public Date date;

    private ArrayList<String> answers;

    public Poll() {
    }

    public Poll(String Question, String Image_URL, ArrayList<String> answers, int vote_count, String creator_ID, String DisplayName, Long epoch, long trend_score, List<String> followers) {
        this.question = Question;
        this.image_URL = Image_URL;
        this.answers = answers;
        this.vote_count = vote_count;
        this.creator_ID = creator_ID;
        this.display_name = DisplayName;
        this.epoch = epoch;
        this.trend_score = trend_score;
        this.date = date;
        this.followers = followers;
    }


    public List<String> getFollowers() {
        return followers;
    }

    public void setFollowers(List<String> followers) {
        this.followers = followers;
    }

    public String getCreator_ID() {
        return creator_ID;
    }

    public void setCreator_ID(String creator_ID) {
        this.creator_ID = creator_ID;
    }

    public String getDisplay_name() {
        return display_name;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getImage_URL() {
        return image_URL;
    }

    public Long getEpoch() {
        return epoch;
    }

    public void setEpoch(Long epoch) {
        this.epoch = epoch;
    }


    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }

    public void setImage_URL(String image_URL) {
        this.image_URL = image_URL;
    }

    public Integer getVote_count() {
        return vote_count;
    }

    public void setVote_count(Integer vote_count) {
        this.vote_count = vote_count;
    }

    public long getTrend_score() {
        return trend_score;
    }

    public void setTrend_score(long trend_score) {
        this.trend_score = trend_score;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("question", question);
        result.put("image_URL", image_URL);
        result.put("vote_count", 0);
        result.put("creator_ID", creator_ID);
        result.put("display_name", display_name);
        result.put("epoch", epoch);
        result.put("trend_score", trend_score);
        result.put("timestamp", FieldValue.serverTimestamp());
        result.put("followers", followers);
        return result;
    }

    @Exclude
    public Map<String, Object> answerConvert(ArrayList<String> answers, int index){
            HashMap<String, Object> result = new HashMap<>();
            result.put("answer", answers.get(index));
            result.put("vote_count", 0);
        return result;
    }

}
