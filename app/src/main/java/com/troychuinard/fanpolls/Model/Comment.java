package com.troychuinard.fanpolls.Model;

import com.google.firebase.firestore.IgnoreExtraProperties;

/**
 * Created by troychuinard on 3/1/16.
 */

@IgnoreExtraProperties
public class Comment {

    private String image_URL;
    private String user_id;
    private String comment;

    public Comment(){

    }

    public Comment(String image_URL, String user_id, String comment) {
        this.image_URL = image_URL;
        this.user_id = user_id;
        this.comment = comment;
    }

    public String getImage_URL() {
        return image_URL;
    }

    public String getUser_id() {
        return user_id;
    }


    public String getComment() {
        return comment;
    }


    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
