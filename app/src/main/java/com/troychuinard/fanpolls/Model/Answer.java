package com.troychuinard.fanpolls.Model;

import com.google.firebase.firestore.IgnoreExtraProperties;

/**
 * Created by troychuinard on 2/12/18.
 */

@IgnoreExtraProperties
public class Answer {

    private String answer;
    private int vote_count;

    public Answer(){

    }

    public Answer(String answer, int vote_count){
        this.answer = answer;
        this.vote_count = vote_count;
    }


    public String getAnswer() {
        return answer;
    }

    public int getVote_count() {
        return vote_count;
    }
}
