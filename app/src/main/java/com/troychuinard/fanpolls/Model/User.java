package com.troychuinard.fanpolls.Model;

import com.google.firebase.firestore.IgnoreExtraProperties;

import java.util.ArrayList;

/**
 * Created by troychuinard on 1/6/16.
 */
@IgnoreExtraProperties
public class User {

    private String email;
    private String display_name;
    private String user_id;
    private ArrayList<String> provider;
    private ArrayList<String> followers;
    private ArrayList<String> following;

    public User(String email, String password) {
        this.email = email;
        this.display_name = display_name;
    }

    public ArrayList<String> getFollowers() {
        return followers;
    }

    public void setFollowers(ArrayList<String> followers) {
        this.followers = followers;
    }

    public ArrayList<String> getFollowing() {
        return following;
    }

    public void setFollowing(ArrayList<String> following) {
        this.following = following;
    }

    public User(){

    }

    public ArrayList<String> getProvider() {
        return provider;
    }

    public void setProvider(ArrayList<String> provider) {
        this.provider = provider;
    }

    public String getEmail() {
        return email;
    }

    public void setEmailAddress(String email) {
        this.email = email;
    }


    public String getDisplay_name() {
        return display_name;
    }

    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }


}

